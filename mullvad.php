<?php
define('MLVD_REQUEST_OVER_PROXY', false);
define('MLVD_ACCT_NUM', '11112222333344445555');// Your Number
define('MLVD_EMERG_KEY', 'wwiirreegguuaarrddPubKey');// Emergency publickey
define('MLVD_EMERG_IP', '10.123.123.123/32');// Emergency IP address
define('MLVD_PRIVATE_DIR', '/tmp/');// where to save private file

function mullvad_api($uri, $method = 'GET', $adata = [])
{
    global $ch, $myMullvadToken;
    if (!isset($ch)) {
        $ch = curl_init();
    }
    curl_setopt($ch, CURLOPT_URL, 'https://api.mullvad.net' . $uri);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 8);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $m);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $myMullvadToken == '' ? ['Accept: application/json', 'Content-Type: application/json'] : ['Accept: application/json', 'Content-Type: application/json', 'Authorization: Bearer ' . $myMullvadToken]);
    curl_setopt($ch, CURLOPT_USERAGENT, 'MullvadApp');
    if (MLVD_REQUEST_OVER_PROXY) {
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
        curl_setopt($ch, CURLOPT_PROXY, '10.64.0.1:1080');
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);
    }
    if ($method != 'GET') {
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($adata));
    }
    $md = curl_exec($ch);
    $mdcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if ($mdcode == 503) {
        sleep(4);
        $md = curl_exec($ch);
    }
    return $md;
}

function mullvad_login()
{
    $knock = @json_decode(mullvad_api('/auth/v1/token', 'POST', ['account_number' => MLVD_ACCT_NUM]), true);
    if (!is_array($knock) || !isset($knock['access_token'])) {
        return false;
    }
    global $myMullvadToken;
    $myMullvadToken = $knock['access_token'];
    return true;
}

function mullvad_list_devices()
{
    return @json_decode(mullvad_api('/accounts/v1/devices'), true);
}

function mullvad_update_pk($your_device_id)
{
    $savemullvad = MLVD_PRIVATE_DIR . 'mullvad.info';
    if (file_exists($savemullvad)) {
        if (gmdate('U', strtotime('24 hour ago')) < filemtime($savemullvad)) {
            return [true, 'Key already rotated recently'];
        }
    } else {
        file_put_contents($savemullvad, json_encode([MLVD_EMERG_KEY, MLVD_EMERG_IP]));
    }
    $res = openssl_pkey_new([
        'private_key_bits' => 256,
        'private_key_type' => OPENSSL_KEYTYPE_EC,
        'curve_name' => 'prime256v1',
    ]);
    openssl_pkey_export($res, $privateKeyString);
    $details = openssl_pkey_get_details($res);
    $publicKeyString = base64_encode($details['key']);
    $knock = @json_decode(mullvad_api('/accounts/v1/devices/' . $your_device_id . '/pubkey', 'PUT', ['pubkey' => $publicKeyString]), true);
    if (!is_array($knock) || !isset($knock['id']) || !isset($knock['ipv4_address'])) {
        return [false, 'cannot update record!'];
    }
    file_put_contents($savemullvad, json_encode([$privateKeyString, $knock['ipv4_address']]));
    return [true, 'Key rotated'];
}

function mullvad_get_login()
{
    return json_decode(file_get_contents(MLVD_PRIVATE_DIR . 'mullvad.info'), true);
}
