1. Save `mullvad.php` to your project
2. `include "mullvad.php";`
3. Use it.

-----

Example

1. Create 2 devices on Mullvad website. (A and B)
2. A is yours and B is for emergency use.
3. Add B's informtion to mullvad.php

```
include "mullvad.php";

mullvad_login();

// Show your device list
print_r( mullavd_list_devices() );

// Generate & Replace the public key
print_r( mullvad_update_pk('mmmyyyyddeevviicceeIdhere') );

// Get assigned private key and IP address
print_r( mullvad_get_login() );

```
